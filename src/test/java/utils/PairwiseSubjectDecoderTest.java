package utils;


import java.io.File;
import java.net.URI;
import java.nio.charset.Charset;

import com.nimbusds.jose.jwk.OctetSequenceKey;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.utils.PairwiseSubjectDecoder;
import com.nimbusds.openid.connect.sdk.id.PairwiseSubjectCodec;
import com.nimbusds.openid.connect.sdk.id.SIVAESBasedPairwiseSubjectCodec;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;


public class PairwiseSubjectDecoderTest extends TestCase {
	

	public void testPrintUsage() {
		
		PairwiseSubjectDecoder.main(new String[]{});
	}
	
	
	public void testJWK()
		throws Exception {
		
		OctetSequenceKey oct = OctetSequenceKey.parse(
			FileUtils.readFileToString(
				new File("src/test/sample-jwk.json"),
				Charset.forName("UTF-8")));
		
		PairwiseSubjectCodec codec = new SIVAESBasedPairwiseSubjectCodec(oct.toSecretKey());
		
		Subject pairwiseSubject = codec.encode(URI.create("https://example.com/sector-id"), new Subject("alice"));
		
		System.out.println("Pairwise subject: " + pairwiseSubject);
		
		PairwiseSubjectDecoder.main(new String[]{"src/test/sample-jwk.json", pairwiseSubject.getValue()});
	}
	
	
	public void testJWKSet()
		throws Exception {
		
		OctetSequenceKey oct = OctetSequenceKey.parse(
			FileUtils.readFileToString(
				new File("src/test/sample-jwk.json"),
				Charset.forName("UTF-8")));
		
		PairwiseSubjectCodec codec = new SIVAESBasedPairwiseSubjectCodec(oct.toSecretKey());
		
		Subject pairwiseSubject = codec.encode(URI.create("https://example.com/sector-id"), new Subject("alice"));
		
		System.out.println("Pairwise subject: " + pairwiseSubject);
		
		PairwiseSubjectDecoder.main(new String[]{"src/test/sample-jwk-set.json", pairwiseSubject.getValue()});
	}
	
	
	public void testInvalidPairwiseID() {
		
		PairwiseSubjectDecoder.main(new String[]{"src/test/sample-jwk-set.json", "invalid-pairwise-id"});
	}
}
