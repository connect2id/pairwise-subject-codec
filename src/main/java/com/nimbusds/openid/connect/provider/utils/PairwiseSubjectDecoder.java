package com.nimbusds.openid.connect.provider.utils;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

import net.minidev.json.JSONObject;
import org.apache.commons.io.FileUtils;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.OctetSequenceKey;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.openid.connect.sdk.id.InvalidPairwiseSubjectException;
import com.nimbusds.openid.connect.sdk.id.PairwiseSubjectCodec;
import com.nimbusds.openid.connect.sdk.id.SIVAESBasedPairwiseSubjectCodec;
import com.nimbusds.openid.connect.sdk.id.SectorID;


/**
 * Pairwise subject (SIV AES) decoder.
 */
public class PairwiseSubjectDecoder {
	
	
	/**
	 * Prints a usage message to the standard output.
	 */
	private static void printUsage() {
	
		System.out.println("Decode pairwise subject (end-user) identifiers issued by Connect2id server 6.x and later versions");
		System.out.println("Usage: java -jar pairwise-subject-codec.jar <jwk-file|jwk-set-file> <pairwise-subject-id>");
		System.out.println("Git repo: https://bitbucket.org/connect2id/pairwise-subject-codec");
		System.out.println("Copyright (c) 2017 - 2021, Connect2id Ltd.");
		System.out.println("Questions? Email support@connect2id.com");
	}
	
	
	/**
	 * The entry point to the command line decoder utility.
	 *
	 * @param args The command line arguments.
	 */
	public static void main(final String[] args) {
	
		if (args == null || args.length != 2) {
			printUsage();
			return;
		}
		
		String content;
		
		try {
			content = FileUtils.readFileToString(new File(args[0]), Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.err.println("Couldn't read JWK / JWK set file: " + e.getMessage());
			return;
		}
		
		JSONObject jsonObject;
		
		try {
			jsonObject = JSONObjectUtils.parse(content);
		} catch (ParseException e) {
			System.err.println("Invalid JSON in JWK / JWK set file: " + e.getMessage());
			return;
		}
		
		final OctetSequenceKey oct;
		
		if (jsonObject.containsKey("keys")) {
			
			JWKSet jwkSet;
			
			try {
				jwkSet = JWKSet.parse(content);
			} catch (java.text.ParseException e) {
				System.err.println("Invalid JWK set: " + e.getMessage());
				return;
			}
			
			JWK jwk = jwkSet.getKeyByKeyId("subject-encrypt");
			
			if (jwk == null) {
				System.err.println("Couldn't find a JWK with key ID \"subject-encrypt\" in the JWK set");
				return;
			}
			
			if (! (jwk instanceof OctetSequenceKey)) {
				System.err.println("The JWK with key ID \"subject-encrypt\" is an octet sequence (secret) key");
				return;
			}
			
			oct = (OctetSequenceKey)jwk;
			
		} else {
		
			try {
				oct = OctetSequenceKey.parse(content);
			} catch (java.text.ParseException e) {
				System.err.println("Invalid octet sequence (secret) key: " + e.getMessage());
				return;
			}
			
			if (! "subject-encrypt".equalsIgnoreCase(oct.getKeyID())) {
				System.err.println("The octet sequence (secret) key ID is not \"subject-encrypt\"");
				return;
			}
		}
		
		
		PairwiseSubjectCodec codec = new SIVAESBasedPairwiseSubjectCodec(oct.toSecretKey());
		
		Subject pairwiseSubject = new Subject(args[1]);
		
		Map.Entry<SectorID,Subject> result;
		
		try {
			result = codec.decode(pairwiseSubject);
		} catch (InvalidPairwiseSubjectException e) {
			System.err.println("Invalid pairwise subject ID: " + e.getMessage());
			return;
		}
		
		System.out.println(result.getValue());
	}
}
