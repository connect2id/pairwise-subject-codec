# Pairwise Subject Decoder

Command-line utility for decoding pairwise subject (end-user) identifiers 
issued by a Connect2id server 6.x or later version.

Usage:

```bash
java -jar pairwise-subject-codec.jar <jwk-file|jwk-set-file> <pairwise-subject-id>
```

Where:

* **jwk** -- A JWK file with the secret key with ID "subject-encrypt" used to 
  encrypt the pairwise identifiers, or
* **jwk-set** -- The JWK set file containing the secret key with ID 
  "subject-encrypt" used to encrypt the pairwise identifiers.
* **pairwise-subject-id** -- The pairwise subject ID to decrypt.

***
Copyright (c) 2017 - 2021, Connect2id Ltd. | [Questions](https://connect2id.com/contact#support)?